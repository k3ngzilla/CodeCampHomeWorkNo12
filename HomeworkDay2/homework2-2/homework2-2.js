let fields = ["id","firstname","lastname","company","salary"];
let employees = [
    ["1001","Luke","Skywalker","Walt Disney","40000"],
    ["1002","Tony","Stark","Marvel","1000000"],
    ["1003","Somchai","Jaidee","Love2work","20000"],
    ["1004","Monkey D","Luffee","One Piece","9000000"]
];

let arr = [];
for (let i=0; i<employees.length; i++){
    let eachEmployees = employees[i]; 
    //console.log(eachEmployees); //ได้ค่าของ Employee แต่ละคน
    let objEmployee = {};
    for (let key in fields){
         //console.log(fields); //ได้ค่า Value ของ Field
        objEmployee[fields[key]] = eachEmployees[key];
    }
    arr.push(objEmployee);
    console.log(arr);
}
