let fs = require('fs');
function readPartHead(){
    let readHead = new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });
    return readHead;
}

function readPartBody(){
    let readBody = new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });
    return readBody;
}

function readPartLeg(){
    let readLeg = new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });
    return readLeg;
}

function readPartFeet(){
    let readFeet = new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });
    return readFeet;
}



async function ReadpartRobot(){
    try {
       let headData = await readPartHead();
       let bodyData = await readPartBody();
       let legData = await readPartLeg();
       let feetData = await readPartFeet();
       let fullRobot = headData + '\n' + bodyData + '\n' + legData + '\n' + feetData;
       fs.writeFile('robot.txt',fullRobot,'utf8',function(err){
            console.log("... Write Complete");
       });     
    } catch (err){
        console.log(err);
    }
}

ReadpartRobot();

// let writefile = function (callback){
//     //data = readfile
//     callback(data);
// }

// writefile(function abc(data){
//     console.log(data);
// });