let fs = require('fs');

let readHead = new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });

let readBody = new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });


let readLeg = new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });

let readFeet = new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, robotData) {
            if (err)
                reject(err);
            else
                resolve(robotData);
        });
    });



Promise.all([readHead, readBody, readLeg, readFeet])
.then(function (result) {
            fs.writeFile('robot.txt', result[0] + '\n' + result[1]+'\n' + result[2]+'\n' + result[3]+'\n', 'utf8', function (err, data) {
            console.log('write complete!!');
            });

    }).catch (function(error){
                console.error("There's an error", error);
            });

            