let employees =
[
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];
let companies = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salaries = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];

let employeesDatabase = [];
for (let i = 0; i < employees.length; i++){
    let employee = employees[i];
    let employeeDetails = {};
    employeeDetails.id = employee.id;
    employeeDetails.firstname = employee.firstname;
    employeeDetails.lastname = employee.lastname;
    for (let j = 0; j < companies.length; j++){
    let company = companies[j];
        if (company.id == employee.id) {
            employeeDetails.company = company.company;
        }
    }
    for (let k = 0; k < salaries.length; k++){
        let emSalary = salaries[k];
        if (emSalary.id == employee.id){
            employeeDetails.emSalary = emSalary.salary;
        }
    }
    //console.log(employeeDetails); ได้ค่าเงินเดือน
    for (let l = 0; l < like.length; l++){
        let emLike = like[i];
        if (emLike.id == employee.id){
            employeeDetails.like = emLike.like;
        }
    }
    //console.log(employeeDetails); ได้ค่า Like
    for (let m = 0; m < dislike.length; m++){
        let emDisklike = dislike[i];
        if(emDisklike.id == employee.id){
            employeeDetails.dislike = emDisklike.dislike;
        }   
    }
    employeesDatabase.push(employeeDetails);
}

temp = JSON.stringify(employeesDatabase);

let fs = require('fs');
fs.writeFile('homework3-3.json', temp, 'utf8', function (err, data) {
    console.log('write complete!!');
});
